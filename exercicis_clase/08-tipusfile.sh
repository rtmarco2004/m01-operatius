#! /bin/bash
# @ edt ASIX-M01 
# Febrer 2023
#
# llistar el directori rebut
# -------------------------------
# 1)Si el numero d'arguments no es correcte plegar
ERR_ARGS=1


if [ $# -ne 1 ]
then
  echo "ERROR: el numero d'arguments no es valid"
  echo "USAGE: $0 dir"
  exit $ERR_ARGS
fi 


# xixa: llistar el dir
ruta=$1
if [ -f $ruta ]
then
  echo "$ruta es un fitxer regular"

elif [ -h $ruta ]
then
  echo "$ruta és un link"

elif [ -d $ruta ]
then
  echo "$ruta és un directori"

elif [ ! -e $ruta ]
then
  echo "$ruta no existeix"

else 
  echo "$ruta es una altra cosa"
fi
exit 0
