#! /bin/bash
# @edt ASIX M01-ISO
# Març 2023
# Exemples case
# -------------------

# programa que ens diu si introduim un dia de la setmana ens diu si es laborable, festiu o altre

case $1 in
  "dilluns"|"dimarts"|"dimecres"|"dijous"|"divendres")
    echo "$1 es un dia laborable"
    ;;
  "dissatbe"|"diumenge")
    echo "$1 es festiu"
    ;;
   *)
    echo "això ($1) no és un dia";;
 
esac
exit 0

# un programa que ens diu si una lletra es una vocal, una consonant o una altra cosa

case $1 in
  [aeiou])
   echo "$1 es una vocal"
   ;;
  [bcdfghjklmnñpqrstvxzwy])
   echo "$1 es una consonant"
   ;;
  *)
    echo "és una altra cosa"
    ;;
esac
exit 0

# un programa que ens diu si el nom introduit es un nen, una nena o no binari

case $1 in
  "pere"|"pau"|"joan")
    echo "és un nen"
    ;;
  "marta"|"anna"|"julia")
    echo "és una nena"
    ;;
  *)
    echo "no binari"
    ;;
esac
exit 0
