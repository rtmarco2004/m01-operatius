#! /bin/bash
# @edt ASIX-M01
# Març 2023
# Descripcio: exemples bucle for
# ---------------------------------

# 8) llistar tots els logins numerats
login=$(cut -d: -f1 /etc/passwd | sort)
contador=1
for argument in $login
do
  echo "$contador: $argument"
  ((contador++))
done
exit 0


# 7) llistar els fitxers del directori actiu i numerats
llistat=$(ls)
contador=1
for argument in $llistat
do
  echo "$contador: $argument"
  ((contador++))
done
exit 0


# 6) numerar els arguments
contador=1
for argument in $*
do
  echo "$contador: $argument" 
  contador=$((contador+1))
done
exit 0


# 5) iterar i mostrar la llista d'arguments 
for argument in "$@"
do
  echo "$argument"
done
exit 0


# 4) iterar i mostrar la llista d'arguments
for argument in $*
do
  echo "$argument"
done
exit 0

# 3) iterar pel valor d'una variable 
llistat=$(ls)
for nom in $llistat
do 
  echo "$nom"
done
exit 0


# 2) iterar per un conjunt d'elements
for nom in "pere pau marta anna"
do
  echo "$nom"
done
exit 0


# 1) iterar per un conjunt d'elements
for nom in "pere" "marta" "pau" "anna"
do
  echo "$nom"
done
exit 0
