#! /bin/bash
# @edt ASIX M01-ISO
# Febrer 2023
# Exemple if: validar la nota si es suspesa, aprobada, notable o execel·lent
#  $ prog edat --> sinopsi
# ------------------------------

# 1)Si numero arguments no es correcte plegar
ERR_NARGS=1
ERR_NOTA=2

if [ $# -ne 1 ]
then

  echo "ERROR: numero arguments incorrecte"
  echo "USAGE: $0 nota"
  exit $ERR_NARGS
fi
# 2)Validar rang nota
if ! [ $1 -ge 0 -a $1 -le 10 ]
then

  echo "ERROR: la nota $1 es incorrecte"
  echo "nota pren valors de 0 a 10"
  echo "USAGE: $0 nota"
  exit $ERR_NOTA
fi

# Xixa 
nota=$1
if [ $nota -lt 5 ]
then 
  echo "la nota $nota està suspesa"
elif [ $nota -lt 7 ]
then
  echo "la nota $nota es un aprovat"

elif [ $nota -lt 9 ]
then
  echo "la nota $nota es un notable"

else
  echo "la nota $nota es un excel·lent"

fi
exit 0
