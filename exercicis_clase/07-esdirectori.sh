#! /bin/bash
# @ edt ASIX-M01 
# Febrer 2023
#
# llistar el directori rebut
# -------------------------------
# 1)Si el numero d'arguments no es correcte plegar
ERR_ARGS=1
ERR_NODIR=2

if [ $# -ne 1 ]
then
  echo "ERROR: el numero d'arguments no es valid"
  echo "USAGE: $0 dir"
  exit $ERR_ARGS
fi 

# 2)si no és un directori plegar 

if [ ! -d $1 ]
then 
  echo "ERROR: $1 no es un directori"
  echo "USAGE: $0 dir"
  exit $ERR_NODIR
fi

# xixa: llistar el dir
dir=$1
ls $dir
exit 
