#! /bin/bash
# @edt ASIX-M01
# Març 2023
# Descripcio: validar que es rep un argument i que és un directori i
# per llistar el coningut amb un simple ls ja n'hi ha prou 
#
# sinopsis: prog dir 
# ---------------------------------
ERR_ARGS=1

if [ $# -eq 0 ]
then
  echo "ERROR: el numero d'arguments no es valid"
  echo "USAGE: $0 dir"
  exit $ERR_ARGS
fi 

# 2)si no és un directori plegar 
for dir in $*
do 
  if  [ ! -d $dir ]
  then 
    echo "$dir no es un directori" 1>&2	
  else
    llistat=$(ls $dir)
    echo "dir: $dir"
    num=1  
    for numerat in $llistat
    do 
      if [ -h $dir/$numerat ]; then
        echo -e "\t $num: $numerat --> es un link"

      elif [ -f $dir/$numerat ]; then
        echo -e "\t $num: $numerat --> es un regular file"

      elif [ -d $dir/$numerat ]; then
        echo -e "\t $num: $numerat --> es un directori"

      else
        echo -e "\t $num: $numerat --> es un altra cosa"
      fi
      num=$(($num+1))
    done	
  fi
done
exit 0
