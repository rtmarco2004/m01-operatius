#! /bin/bash
# @edt ASIX-M01
# Març 2023
# Descripcio: dir els dies que té un més
# Synopsis: prog mes
# ---------------------------------
ERR_NARG=1
ERR_ARGVL=2

if [ $# -ne 1 ]
then
  echo "ERROR: el numero d'arguments introduit no es valid"
  echo "USAGE: $0 mes"
  exit $ERR_NARG

fi

mes=$1

if ! [ $mes -ge 1 -a $mes -le 12 ]
then
  echo "Error, mes $mes no vàlid"
  echo "Mes pren valors del [1-12]"
  echo "Usage: $0 mes"
  exit $ERR_ARGVL
fi


case "$mes" in
  "2")
    dies=28;;
  "4"|"6"|"9"|"11")
    dies=30;;
  *)
    dies=31;;

esac    
echo "El més: $mes, té $dies dies"
exit 0
 
