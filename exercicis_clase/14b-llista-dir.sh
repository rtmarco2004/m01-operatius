#! /bin/bash
# @edt ASIX-M01
# Març 2023
# Descripcio: llistar dir i numerar elements 
#
# sinopsis: prog dir 
# ---------------------------------
ERR_ARGS=1
ERR_NODIR=2

if [ $# -ne 1 ]
then
  echo "ERROR: el numero d'arguments no es valid"
  echo "USAGE: $0 dir"
  exit $ERR_ARGS
fi 

# 2)si no és un directori plegar 

if  [ ! -d $1 ]
then 
  echo "ERROR: $1 no es un directori"
  echo "USAGE: $0 dir"
  exit $ERR_NODIR
fi

#fem ls del directori
contador=1
dir=$1
llista=$(ls $dir) 

for numerat in $llista 
do 

  echo "$contador: $numerat"
  ((contador++))

done
exit 0

