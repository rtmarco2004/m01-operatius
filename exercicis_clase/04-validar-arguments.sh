#! /bin/bash
# @ edt ASIX-M01 
# Febrer 2023
# Validar que té exàctament 2 args
# i mostarr-los
# -------------------------------
# 1)si num args no es correcte plegar

if [ $# -ne 2 ]
then 
  echo "ERROR: numero arguments incorrecte"
  echo "USAGE: $0 nom edat"
  exit 1 
fi

# 2)xixa
echo "nom: $1"
echo "edat: $2"
exit 0
