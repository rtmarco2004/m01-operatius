#! /bin/bash
# @edt ASIX-M01
# Març 2023
# Descripcio: validar que es rep un argument i que és un directori i
# per llistar el coningut amb un simple ls ja n'hi ha prou 
#
# sinopsis: prog dir 
# ---------------------------------
ERR_ARGS=1
ERR_NODIR=2

if [ $# -ne 1 ]
then
  echo "ERROR: el numero d'arguments no es valid"
  echo "USAGE: $0 dir"
  exit $ERR_ARGS
fi 

# 2)si no és un directori plegar 

if  [ ! -d $1 ]
then 
  echo "ERROR: $1 no es un directori"
  echo "USAGE: $0 dir"
  exit $ERR_NODIR
fi

#fem ls del directori

dir=$1

ls $dir 
exit 0

