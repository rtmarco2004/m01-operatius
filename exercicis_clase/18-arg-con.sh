#! /bin/bash
#@ edt ASIX M01
#
# 18-arg-con.sh [-a -b -c ]
#----------------------------------------------------------------
opcions=""
arguments=""
myfile=""
num=""

while [ -n "$1" ]
do
  case "$1" in 
    "-a"|"-b"|"-c")
      opcions="$opcions $1";;
    "-f")
      myfile="$myfile $2"
      shift;;
    "-n")
      num="$num $2"
      shift;;
    *)
      arguments="$arguments $1"
  esac
  shift
done 
echo "opcions: $opcions"
echo "file: $myfile"
echo "arguments: $arguments"
echo "nums: $num"
