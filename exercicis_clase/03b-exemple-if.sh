#! /bin/bash
# @edt ASIX M01-ISO
# Febrer 2023
# Exemple if: indicar si és major o menor d'edat
#  $ prog edat --> sinopsi
# ------------------------------

# 1) Validar que existeix un argument
if [ $# -ne 1 ]
then
  echo "Error: numero arguments incorrecte"
  echo "Usage: $0 edat"
  exit 1
fi

# 2) xixa
edat=$1
if [ $edat -lt 18 ]
then
  echo "edat $edat és menor d'edat"
else 
  echo "edat $edat és major d'edat"

fi
exit 0

