#! /bin/bash
# @edt ASIX-M01
# Març 2023
# Descripcio: indicar per cada nota introduida la qualificacio que te.
# ---------------------------------
ERR_ARGS=1
ERR_NOTA=2
if [ $# -lt 1 ]; then
  echo "ERROR: num args incorrecte"
  echo "usage: $0 num"
  exit $ERR_ARGS
fi

for nota in "$@"

do 

if ! [ $nota -ge 0 -a $nota -le 10 ]
then
  echo "ERROR: la nota $nota es incorrecte"
  echo "Nota pren valors del 0 al 10"
  echo "USAGE: $0 nota"
  exit ERR_NOTA
fi 


if [ $nota -lt 5 ] 
then
  resultat="suspes"

elif [ $nota -lt 7 ]
then
  resultat="aprovat"

elif [ $nota -lt 9 ]
then
  resultat="notable"

else
  resultat="execel·lent"
 
fi

echo "La nota $nota és un $resultat"

 
done 
