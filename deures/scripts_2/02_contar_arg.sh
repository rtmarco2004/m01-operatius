total=0
for paraula in $*
do
  contador=$(echo $paraula | wc -c)
  if [ $contador -ge 4 ]; then
    ((total++))
  fi
done
echo "$total"
