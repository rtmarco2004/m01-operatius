errors=0
for matricula in $*
do
  comprobador=$(echo $matricula | grep -E "^[0-9]{4}-[A-Z]{3}$")
  if ! [ $? -eq 0 ]; then
    ((errors++))
  fi
done
echo "matricules fallades: $errors"
exit 0
	  
