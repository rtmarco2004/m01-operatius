#! /bin/bash

contador=0
while read -r line
do
  contador=$((contador+1))
  echo $contador':' $line | tr 'a-z' 'A-Z'
done 

