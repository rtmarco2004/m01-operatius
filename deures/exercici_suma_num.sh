#! /bin/bash
# @edt ASIX-M01
# Març 2023
# Descripcio: suma de nombres
# ---------------------------------

suma=0

for num in "$@"
do
  suma=$((suma + num))
done 

echo "La suma total es: $suma"
