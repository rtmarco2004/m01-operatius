ERRARG=1 
ERR_DIR=2

if [ $# -eq 0 ]
then
  echo "ERROR: numero d'arguments incorrecte"
  echo "usage: $0 dir"
  exit $ERRARG
fi

for dir in $*
do
  if  ! [ -d $dir ]
then
    echo "$dir no es un directori" >> /dev/stderr
    else 
      llista=$(ls $dir)
      echo "dir: $dir"
      num=1 
      for valor in $llista 
      do
        if [ -h $dir/$valor ]; then
          echo -e  "\t$num: $valor es un --> link"
        elif [ -d $dir/$valor ]; then
          echo -e "\t$num: $valor es un --> directori"
        elif [ -f $dir/$valor ]; then
          echo -e "\t$num: $valor es un --> regular file"
        else
          echo -e "\t$num: $valor es una altra cosa"
        fi
	num=$(($num + 1))
      done 
  fi
done
exit 0
