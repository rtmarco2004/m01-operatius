#! /bin/bash
# @edt ASIX
# Març 2023
# Descripcio: compator zero - arg
# -------------------------------------------------------------------
num=0
MAX="$1"
while [ $num -le $MAX ]
do	
  echo "$num"
  num=$((num++))  
done

exit 0 
