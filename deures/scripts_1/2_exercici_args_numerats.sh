#! /bin/bash
# @edt ASIX 
# Març 2023 
# Descripcio: mostrar els args numerats
# -------------------------------------------------------------------
num=1
for arg in $*
do	
  echo "$num: $arg"
  num=$((num+1))  
done

exit 0 
