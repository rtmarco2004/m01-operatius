#! /bin/bash
# @edt ASIX 
# Març 2023
# Descripcio: mostrar l'entrada rnomés 50 primer caràcters
# -------------------------------------------------------------------
while read -r line
do	
  echo $line | cut -c1-50
done

exit 0 
