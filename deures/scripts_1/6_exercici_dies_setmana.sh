#! /bin/bash
# @edt ASIX-M01
# Març 2023
# Descripcio: dies de la setmana que ens diu si son laborables o no
# ---------------------------------

laborable=0
festiu=0

for dia in $*

do 

case $dia in 
  dilluns|dimarts|dimecres|dijous|divendres) 
	  laborable=$((laborable+1))
;;
  dissabte|diumenge)
	  festiu=$((festiu+1))
;;
*) echo "El dia $dia no serveix" >> /dev/stderr
;;
esac
done
echo "Dies laborables = $laborable"
echo "Dies festius = $festiu"

